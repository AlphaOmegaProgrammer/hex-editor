This was my practice for doing [AoC 2023 in raw machine code](https://gitlab.com/AlphaOmegaProgrammer/aoc-2023-raw-machine-code). I was writing the hex editor in a hex editor. I didn't get very far.

I may revisit this at some point in the future. Being able to write a hex editor in a hex editor is, in my opinion, the ultimate toolchain, so I'd like to be able to do this and document how others can do it in a way that leads to being able to bootstrap any environment they like.
